import request from '@/utils/request'

//获取受访页面列表
export function getVisitPageList(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/visit/pages',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId
         }
    })
}

//获取趋势总数居
export function getTrendTotal(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/trend/total',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId
         }
    })
}

//获取趋势天列表
export function getTrendDayList(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/trend/day/list',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId
         }
    })
}

//趋势-导出明细
export function exportDetail(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/trend/detail/export',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId
         }
    })
}

//地域分布-总数
export function getRegionLayoutTotal(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/region/layout',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId
         }
    })
}

//地域分布-浏览量-列表
export function getRegionLayoutPvList(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/region/layout/list',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId,
            data_type:1
         }
    })
}

//地域分布-访客数-列表
export function getRegionLayoutUvList(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/region/layout/list',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId,
            data_type:2
         }
    })
}

//地域分布-pv/uv/ip-列表
export function getRegionPvUvIpList(dateStart,dateEnd,sourceTypeId,deviceTypeId,regionName,visitorTypeId) {
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/region/pvuvip/list',
         method: 'post',
         data:{
            start_date:dateStart,
            end_date:dateEnd,
            source_type:sourceTypeId,
            os_type:deviceTypeId,
            province:regionName,
            visitor_type:visitorTypeId
         }
    })
}

//获取地域列表
export function getRegionList() {    
    return request({
        // baseURL:'http://127.0.0.1:19502/api',
         url: '/region/list',
         method: 'post',
         data:{
         }
    })
}

