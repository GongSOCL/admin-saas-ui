import request from '@/utils/request'

//搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/search',
    method: 'post',
    data
  })
}

//上架
export function publishAct(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/publishAct',
    method: 'put',
    data
  })
}

//下架
export function downAct(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/downAct',
    method: 'put',
    data
  })
}

//删除
export function del(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/del',
    method: 'put',
    data
  })
}

//编辑
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/update',
    method: 'post',
    data
  })
}

//详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/detail',
    method: 'post',
    data
  })
}

//types
export function types(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/types',
    method: 'get'
  })
}

//lists
export function getActList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/getActList',
    method: 'get',
    data
  })
}

//分配
export function distribution(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/distribution',
    method: 'post',
    data
  })
}

//分配
export function end(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/end',
    method: 'post',
    data
  })
}

//set
export function set(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/activity/set',
    method: 'post',
    data
  })
}
