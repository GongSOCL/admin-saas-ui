import request from '@/utils/request'

export function fetchRoutes() {
  return request({
    url: '/manage/routes',
    method: 'get'
  })
}

export function getRole(id) {
  return request({
    url: `/manage/roles/${id}`,
    method: 'get'
  })
}

export function getRoles(query) {
  return request({
    url: '/manage/roles',
    method: 'get',
    params: query
  })
}

export function createRole(data) {
  return request({
    url: '/manage/roles',
    method: 'post',
    data
  })
}

export function updateRole(id, data) {
  return request({
    url: `/manage/roles/${id}`,
    method: 'put',
    data
  })
}

export function deleteRole(id) {
  return request({
    url: `/manage/roles/${id}`,
    method: 'delete'
  })
}

// 根据角色ID查询菜单下拉树结构
export function getRoleMenu(id) {
  return request({
    url: `/manage/roles/menu/${id}`,
    method: 'get'
  })
}

export function updateRoleMenu(id, data) {
  return request({
    url: `/manage/roles/menu/${id}`,
    method: 'put',
    data
  })
}

export function searchRoles(keywords = "", limits = 10) {
  return request({
    url: `/manage/roles/search`,
    method: 'get',
    params: {
      keywords,
      limits
    }
  })
}

export function toggleRoleStatus(roleId) {
  return request({
    url: `/manage/roles/toggle-status/${roleId}`,
    method: 'post'
  })
}

