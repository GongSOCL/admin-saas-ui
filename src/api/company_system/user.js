import request from '@/utils/request'

export function getUsers(query) {
  return request({
    url: '/system/users',
    method: 'get',
    params: query
  })
}

export function updateUser(id, data) {
  return request({
    url: `/system/user/${id}`,
    method: 'put',
    data
  })
}

export function getUser(id, data) {
  return request({
    url: `/system/user/${id}`,
    method: 'get',
    data
  })
}

export function createUser(data) {
  return request({
    url: '/system/user',
    method: 'post',
    data
  })
}

export function deleteUser(id) {
  return request({
    url: `/manage/user/${id}`,
    method: 'delete'
  })
}

export function avatarUpload(data) {
  return request({
    url: '/manage/user/avatar-upload',
    method: 'post',
    data
  })
}

export function getCompanys() {
  return request({
    url: '/manage/user/companys',
    method: 'get'
  })
}
