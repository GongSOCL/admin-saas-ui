import request from '@/utils/request'

export function getRoutes() {
  return request({
    url: '/manage/routes',
    method: 'get'
  })
}
