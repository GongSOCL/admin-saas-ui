import request from '@/utils/request'

export function listEmployee(current = 1, limit = 10, company_id = 0) {
     return request({
          url: '/company/employees',
          method: 'get',
          params: {
               current,
               limit,
               company_id
          }
      })
}

export function infoEmployee(id) {
     return request({
          url: `/company/employees/${id}`,
          method: 'get',
      })
}


export function addEmployee(data) {
     return request({
          url: 'company/employees',
          method: 'put',
          data: data
      })
}

export function editEmployee(id, data) {
     return request({
          url: `/company/employees/${id}`,
          method: 'post',
          data: data
      })
}

export function toggleStatus(id) {
     return request({
          url: `/company/employees/status/${id}`,
          method: 'post',
      })
}

// 重置密码
export function resetPassword(id) {
     return request({
          url: `/company/employees/reset-password/${id}`,
          method: 'post',
      })
}