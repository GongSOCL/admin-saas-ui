import request from '@/utils/request'

export function listCompany(current = 1, limit = 10) {
     return request({
          url: '/company',
          method: 'get',
          params: {
               current,
               limit
          }
      })
}

export function infoCompany(id) {
     return request({
          url: `/company/${id}`,
          method: 'get',
      })
}


export function addCompany(data) {
     return request({
          url: '/company',
          method: 'put',
          data: data
      })
}

export function editCompany(id, data) {
     return request({
          url: `/company/${id}`,
          method: 'post',
          data: data
      })
}

export function toggleStatus(id) {
     return request({
          url: `/company/status/${id}`,
          method: 'post',
      })
}

export function searchCompany(keywords = "", limits = 10) {
     return request({
       url: '/company/search',
       method: 'get',
       params: {
         keywords,
         limits
       }
     })
   }
