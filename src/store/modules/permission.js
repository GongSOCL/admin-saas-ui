import { constantRoutes } from '@/router'
import { getMenus } from '@/api/user'
/* Layout */
import Layout from '@/layout'
import { Message } from 'element-ui'

export const loadView = (view) => { // 路由懒加载
  return (resolve) => require(['@/views/' + view], resolve)
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param data
 */
export function formatRoutes(data) {
  const res = []
  data.forEach(item => {
    const menu = {
      path: item.path,
      component: item.component === '' ? Layout : loadView(item.component),
      hidden: item.status === 0,
      name: item.name,
      meta: {
        title: item.title,
        icon: item.icon
      },
      children: []
    }

    if (item.children && item.children.length > 0) {
      menu.children = formatRoutes(item.children)
    }
    res.push(menu)
  })

  return res
}

const state = {
  routes: [],
  addRoutes: [],
  actions: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  },
  SET_ACTIONS: (state, actions) => {
    state.actions = actions
  }
}

const actions = {
  generateRoutes({ commit }) {
    return new Promise(resolve => {
      getMenus().then(res => {
        const accessedRoutes = formatRoutes(res.data.menus)
        // 404 page must be placed at the end !!!
        accessedRoutes.push({ path: '*', redirect: '/404', hidden: true })

        commit('SET_ROUTES', accessedRoutes)
        commit('SET_ACTIONS', res.data.actions)

        resolve(accessedRoutes)
      }).catch(error => {
        Message.error(error)
      })
    })
  },

  // remove token
  resetRoute({ commit, state }) {
    return new Promise(resolve => {
      commit('SET_ROUTES', [])
      commit('SET_ACTIONS', [])
      resolve(...state.routes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
